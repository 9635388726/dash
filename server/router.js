const { Router } = require('express')
const mongoose = require('mongoose')
const Model = require('./models/advise')
const round = require('mongo-round')

const router = Router()

const advises = []
const score = []
const db = 'mongodb://127.0.0.1:27017/dash'

mongoose.connect(db, (err, res) => {
    if (err) {
        console.log('Failed to connected to ' + db)
    } else {
        console.log('Connected to ' + db)
    }
})

// GET
router.get('/api/advises' , function(req, res){
    Model.find({} , (err, advises) => {
        if(err){
            res.status(404).send(err)
        }else {
            res.status(200).send(advises)
        }
    })
})

// GET
router.get('/api/advises/average' , function(req, res){
   Model.aggregate([
        {
            $group: {
                _id: null,
                scoreCalc: { $avg: "$score" },
            }
        }
    ],  

   (err, avg) => {
        if(err){
            res.status(404).send(err)
        }else {
            const result = Math.round(avg[0].scoreCalc * 100) / 100;
            res.status(200).send(JSON.stringify(result))
        }
    })
})

router.get('/api/advises/total' , function(req, res){
  Model.countDocuments({} , (err, advises) => {
    res.status(200).send(JSON.stringify(advises))
  })
})

router.get('/api/advises/total-comments' , function(req, res){
  Model.countDocuments({"comment":{$exists:true}} , (err, advises) => {
    res.status(200).send(JSON.stringify(advises))
  })
})

router.get('/api/advises/top-10' , function(req, res){
  Model.find({}, "siret score").sort({score: 'descending'}).limit(10).exec(function(err, advises) {
    res.status(200).send(advises)
  })
})

// post
router.post('/api/advises', function(req, res){
    let model = new Model(req.body)
    model.save()
    res.status(201).send(model)
})

// respond with "hello world" when a GET request is made to the homepage
router.get('/api/hello', function(req, res) {
  res.send('hello world')
})

console.log(router)
module.exports = router
